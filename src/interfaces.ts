import {
  faDroplet,
  faWorm,
  faSeedling,
  faCow,
  faDragon,
  IconDefinition,
  faCircle,
  faTablets,
} from "@fortawesome/free-solid-svg-icons";

export type RecourceType = "water" | "nutri" | "plants" | "prey" | "carnivore";
export type UpgradeType =
  | "addRoots"
  | "addPlants"
  | "addHerbivore"
  | "addCarnivore";

export interface IUpgrade {
  name: string;
  id: UpgradeType;
  icon: TIcon;
  isLocked: boolean;
  upgradesTillUnlocked: number;
  description: string;
  cost: Cost;
  effect: Effect;
}

interface Cost {
  water: number;
  nutri: number;
  plants: number;
  prey: number;
  carnivore: number;
}

export type Effect = {
  water?: {
    amount: number;
    type: "overtime" | "instant";
    visibleInUpgrade: boolean;
  };
  nutri?: {
    amount: number;
    type: "overtime" | "instant";
    visibleInUpgrade: boolean;
  };
  plants?: {
    amount: number;
    type: "overtime" | "instant";
    visibleInUpgrade?: boolean;
  };
  prey?: {
    amount: number;
    type: "overtime" | "instant";
    visibleInUpgrade?: boolean;
  };
  carnivore?: {
    amount: number;
    type: "overtime" | "instant";
    visibleInUpgrade?: boolean;
  };
};

export interface IResource {
  type: RecourceType;
  amount: number;
  cooldown: number;
  locked: boolean;
}

export interface IState {
  water: IResource;
  nutri: IResource;
  plants: IResource;
  prey: IResource;
  carnivore: IResource;
  rings: number[];
  upgrades: TUpgrades;
}

type TIcon = {
  icon: IconDefinition;
  color: string;
};

export type TUpgrades = {
  addRoots: IUpgrade;
  addPlants: IUpgrade;
  addHerbivore: IUpgrade;
  addCarnivore: IUpgrade;
};

export const mapping = {
  water: {
    icon: faDroplet,
    color: "text-blue-300",
  },
  nutri: {
    icon: faTablets,
    color: "text-orange-300",
  },
  plants: {
    icon: faSeedling,
    color: "text-green-300",
  },
  prey: {
    icon: faCow,
    color: "text-yellow-300",
  },
  carnivore: {
    icon: faDragon,
    color: "text-red-300",
  },
  root: {
    icon: faCircle,
    color: "text-blue-300",
  },
};
