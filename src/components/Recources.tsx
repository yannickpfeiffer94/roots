import { naiveRound } from "@/helpers";
import { IResource, IState, RecourceType, mapping } from "@/interfaces";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

interface Props {
  dispatch: React.Dispatch<any>;
  state: IState;
}

export function Recources({ dispatch, state }: Props) {
  React.useEffect(() => {
    function createInterval(type: RecourceType) {
      return setInterval(() => {
        dispatch({ actionType: "updateResource", resourceType: type });
      }, state[type].cooldown * 1000);
    }

    const waterInterval = createInterval("water");
    const nutriInterval = createInterval("nutri");
    const plantsInterval = createInterval("plants");
    const preyinterval = createInterval("prey");
    const carnivoreInterval = createInterval("carnivore");
    return () => {
      clearInterval(waterInterval);
      clearInterval(plantsInterval);
      clearInterval(nutriInterval);
      clearInterval(preyinterval);
      clearInterval(carnivoreInterval);
    };
  }, [dispatch]);

  return (
    <div className="w-1/2 flex flex-col gap-2">
      <Recource resource={state.water} />
      <Recource resource={state.nutri} />
      <div className="mt-auto">
        <Recource resource={state.plants} />
        <Recource resource={state.prey} />
        <Recource resource={state.carnivore} />
      </div>
    </div>
  );
}

interface RecourceProps {
  resource: IResource;
}

function Recource({ resource }: RecourceProps) {
  if (resource.locked) return null;
  return (
    <div className={`p-4 flex gap-3 items-center`}>
      <FontAwesomeIcon
        className={`text-4xl w-16 transition-all ${
          mapping[resource.type].color
        }`}
        icon={mapping[resource.type].icon}
      />
      <div className="flex items-center gap-2">
        <span className="font-black text-xl" style={{ minWidth: "35px" }}>
          {naiveRound(resource.amount, 1)}
        </span>
        <span className="opacity-80 text-sm">+1/sec </span>
      </div>
    </div>
  );
}
