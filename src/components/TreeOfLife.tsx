import { IState } from "@/interfaces";
import React from "react";

interface Props {
  state: IState;
}

export function TreeOfLife({ state }: Props) {
  return (
    <>
      {state.rings.map((ring, index) => {
        return <Ring key={index} index={index} />;
      })}
    </>
  );
}

function Ring({ index }: { index: number }) {
  const firstRender = React.useRef(true);

  React.useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
      return;
    }
  }, []);

  return (
    <span
      key={index}
      className={`opacity-30 ml-8 absolute h-20 bg-blue-200 rotate-45 top-[50%] ease-out duration-300 transition-all -translate-y-1/2 left-[50%] -translate-x-1/2 rounded-full
        ${firstRender.current ? "scale-0" : "scale-100"}
      `}
      style={{
        width: 40 * (index + 1) + "px",
        height: 40 * (index + 1) + "px",
      }}
    ></span>
  );
}
