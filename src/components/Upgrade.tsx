import { naiveRound } from "@/helpers";
import { IState, IUpgrade, RecourceType, mapping } from "@/interfaces";
import {
  faAngleDown,
  faAnglesDown,
  faAnglesUp,
  faMinus,
  faPlus,
  faSeedling,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

interface Props {
  upgrade: IUpgrade;
  state: IState;
  dispatch: React.Dispatch<any>;
}

export function Upgrade({ upgrade, state, dispatch }: Props) {
  const [isAfordable, setIsAfordable] = React.useState(false);

  React.useEffect(() => {
    if (
      state.water.amount >= upgrade.cost.water &&
      state.nutri.amount >= upgrade.cost.nutri &&
      state.plants.amount >= upgrade.cost.plants &&
      state.prey.amount >= upgrade.cost.prey &&
      state.carnivore.amount >= upgrade.cost.carnivore
    ) {
      setIsAfordable(true);
      return;
    } else {
      setIsAfordable(false);
    }
  }, [state, setIsAfordable, upgrade.cost]);

  function handleClick() {
    if (!isAfordable) return;
    dispatch({ actionType: "upgrade", upgrade: upgrade });
    dispatch({ actionType: "unlockUpgrade", upgrade: upgrade });
  }

  return (
    <button
      disabled={!isAfordable}
      onClick={handleClick}
      className={`p-3 flex bg-[#1e1e1e] items-center transition-all duration-500 ease-in-out gap-3 rounded flex-row 
      ${!isAfordable ? "opacity-50" : "hover:bg-opacity-80"}
      ${upgrade.isLocked ? "translate-x-[120%]" : "translate-x-0"}
        `}
    >
      <div className="rounded-full">
        <FontAwesomeIcon
          className={`text-2xl px-2.5 ${upgrade.icon.color}`}
          icon={upgrade.icon.icon}
        />
      </div>
      <div className="flex flex-col w-full">
        <div className="flex flex-row w-full">
          <span className="opacity-50">{upgrade.name}</span>

          {/* Costs  */}
          <ul className="flex flex-row ml-auto gap-3">
            {Object.entries(upgrade.cost).map((cost, index) => {
              const costType: RecourceType = cost[0] as RecourceType;
              const value: number = cost[1];

              if (value === 0) return null;

              return (
                <li key={index} className="gap-1 flex items-center text-lg">
                  <div className="flex items-center gap-1">
                    <FontAwesomeIcon
                      className={`${mapping[costType].color}`}
                      icon={mapping[costType].icon}
                    />
                    <span>{naiveRound(value, 0)}</span>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>

        {/* Effects  */}
        <ul className="flex items-center flex-row gap-4">
          {Object.entries(upgrade.effect).map((effect, index) => {
            const effectType: RecourceType = effect[0] as RecourceType;
            const value = effect[1];

            if (value.type === "instant") return null;

            return (
              <li key={index} className="flex items-center gap-1.5 mt-0.5">
                <FontAwesomeIcon
                  className="text-base"
                  icon={value.amount > 0 ? faAnglesUp : faAngleDown}
                />
                <FontAwesomeIcon
                  className={`${mapping[effectType].color}`}
                  icon={mapping[effectType].icon}
                />
              </li>
            );
          })}
        </ul>
      </div>
    </button>
  );
}
