import React from "react";

export function Overlay() {
  const firstRender = React.useRef(true);
  const [start, setStart] = React.useState(false);

  React.useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
      return;
    }
  }, []);

  function handleStart() {
    const button = document.getElementById("startButton");
    const overlayContainer = document.getElementById("overlayContainer");

    setStart(true);

    button?.addEventListener("transitionend", (e) => {
      if (e.propertyName !== "transform") return;
      overlayContainer?.classList.add("opacity-0");
      overlayContainer?.classList.add("translate-y-[100%]");
      setStart(false);
    });
  }

  return (
    <div
      id="overlayContainer"
      className={`bg-[#1e1e1e] flex transition-all duration-[0.45s] fixed inset-0 opacity-100`}
    >
      <div className="mx-auto my-auto flex flex-col items-center">
        <span className="mx-auto my-auto text-4xl font-semibold">
          Tree of life
        </span>
        <span className="mx-auto my-auto text-lg mt-2 opacity-80">
          Grow your tree, balance resources, create life. Welcome to Tree of
          Life!
        </span>
        <button
          id="startButton"
          onClick={handleStart}
          className={`w-20 h-20 mt-6 rounded-full bg-blue-300  ease-in-out transition-all ${
            start
              ? "scale-[2500%] duration-[1.25s]"
              : "scale-100 duration-1000 "
          } `}
        ></button>
      </div>
    </div>
  );
}
