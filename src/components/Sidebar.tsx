import { faAnglesRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

export function SideBar() {
  const [isOpen, setIsOpen] = React.useState(false);

  return (
    <>
      <section
        className={`right-0 top-0 bottom-0 absolute w-1/4 bg-[#333333] transition-all ${
          isOpen ? "translate-x-0" : "translate-x-full"
        }`}
      >
        <button
          onClick={() => setIsOpen(!isOpen)}
          className={`bg-[#252526] absolute top-4 w-8 h-8 transition-all rounded-full ${
            isOpen ? "-left-4" : "-left-12"
          }`}
        >
          <div className="flex flex-row">
            <FontAwesomeIcon
              className={`text-whitetext-sm p-1.5 translate-all ${
                isOpen ? "rotate-0" : "rotate-180"
              }`}
              icon={faAnglesRight}
            />
          </div>
        </button>
      </section>
    </>
  );
}
