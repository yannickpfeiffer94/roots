import { IResource, IState, IUpgrade } from "@/interfaces";
import { Upgrade } from "./Upgrade";

interface Props {
  state: IState;
  dispatch: React.Dispatch<any>;
}

export function UpgradeBox({ state, dispatch }: Props) {
  return (
    <div className="bg-[#333333] w-1/3 flex flex-col gap-2.5 p-4">
      {Object.entries(state.upgrades).map(([key, value]) => {
        return (
          <Upgrade
            key={key}
            upgrade={value}
            state={state}
            dispatch={dispatch}
          />
        );
      })}
      <div className="text-2xl flex flex-col text-center mt-auto mb-8">
        <span className="font-semibold">Tree of life</span>
        <span className="text-base opacity-70">
          Grow your tree, balance resources, create life
        </span>
      </div>
    </div>
  );
}
