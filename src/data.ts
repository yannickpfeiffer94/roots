import { IState, TUpgrades, mapping } from "./interfaces";

export const upgrades: TUpgrades = {
  addRoots: {
    name: "Add root",
    id: "addRoots",
    icon: mapping.root,
    isLocked: false,
    upgradesTillUnlocked: 0,
    description: "Add more roots to the tree",
    cost: {
      water: 5,
      nutri: 5,
      plants: 0,
      prey: 0,
      carnivore: 0,
    },
    effect: {
      water: {
        amount: 0.3,
        type: "overtime",
        visibleInUpgrade: true,
      },
      nutri: {
        amount: 0.3,
        type: "overtime",
        visibleInUpgrade: true,
      },
    },
  },
  addPlants: {
    name: "Add plant",
    id: "addPlants",
    icon: mapping.plants,
    isLocked: true,
    upgradesTillUnlocked: 2,
    description: "Add more plants to the tree",
    cost: {
      water: 7,
      nutri: 7,
      plants: 0,
      prey: 0,
      carnivore: 0,
    },
    effect: {
      water: {
        amount: -0.1,
        type: "overtime",
        visibleInUpgrade: true,
      },
      nutri: {
        amount: -0.1,
        type: "overtime",
        visibleInUpgrade: true,
      },
      plants: {
        amount: 1,
        type: "instant",
        visibleInUpgrade: false,
      },
    },
  },
  addHerbivore: {
    name: "Add herbivore",
    id: "addHerbivore",
    icon: mapping.prey,
    isLocked: true,
    upgradesTillUnlocked: 4,
    description: "Adds a cow that chews plants",
    cost: {
      water: 0,
      nutri: 0,
      plants: 1,
      prey: 0,
      carnivore: 0,
    },
    effect: {
      prey: {
        amount: 1,
        type: "instant",
        visibleInUpgrade: false,
      },
      plants: {
        amount: -0.1,
        type: "overtime",
        visibleInUpgrade: true,
      },
    },
  },
  addCarnivore: {
    name: "Add carnivore",
    id: "addCarnivore",
    icon: mapping.carnivore,
    isLocked: true,
    upgradesTillUnlocked: 7,

    description: "Adds a cow that chews plants",
    cost: {
      water: 0,
      nutri: 0,
      plants: 0,
      prey: 1,
      carnivore: 0,
    },
    effect: {
      carnivore: {
        amount: 1,
        type: "instant",
        visibleInUpgrade: false,
      },
      prey: {
        amount: -0.1,
        type: "overtime",
        visibleInUpgrade: true,
      },
    },
  },
};

export const initialState: IState = {
  water: {
    type: "water",
    amount: 0,
    cooldown: 1.1,
    locked: false,
  },
  nutri: {
    type: "nutri",
    amount: 0,
    cooldown: 1.2,
    locked: false,
  },
  plants: {
    type: "plants",
    amount: 0,
    cooldown: 10,
    locked: true,
  },
  prey: {
    type: "prey",
    amount: 0,
    cooldown: 20,
    locked: true,
  },
  carnivore: {
    type: "carnivore",
    amount: 0,
    cooldown: 30,
    locked: true,
  },
  rings: [1],
  upgrades: upgrades,
};
