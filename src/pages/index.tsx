import { Overlay } from "@/components/Overlay";
import { Recources } from "@/components/Recources";
import { TreeOfLife } from "@/components/TreeOfLife";
import { UpgradeBox } from "@/components/Upgradebox";
import { initialState } from "@/data";
import { IState } from "@/interfaces";
import React, { useReducer } from "react";

function reducer(state: any, action: any) {
  switch (action.actionType) {
    case "unlockUpgrade":
      console.log(state);
      return {
        ...state,
        upgrades: {
          ...state.upgrades,
          addPlants: {
            ...state.upgrades.addPlants,
            upgradesTillUnlocked:
              state.upgrades.addPlants.upgradesTillUnlocked - 1,
            isLocked:
              state.upgrades.addPlants.upgradesTillUnlocked - 1 <= 0
                ? false
                : true,
          },
          addHerbivore: {
            ...state.upgrades.addHerbivore,
            upgradesTillUnlocked:
              state.upgrades.addHerbivore.upgradesTillUnlocked - 1,
            isLocked:
              state.upgrades.addHerbivore.upgradesTillUnlocked - 1 <= 0
                ? false
                : true,
          },
          addCarnivore: {
            ...state.upgrades.addCarnivore,
            upgradesTillUnlocked:
              state.upgrades.addCarnivore.upgradesTillUnlocked - 1,
            isLocked:
              state.upgrades.addCarnivore.upgradesTillUnlocked - 1 <= 0
                ? false
                : true,
          },
        },
        plants: {
          ...state.plants,
          locked:
            state.upgrades.addPlants.upgradesTillUnlocked - 1 <= 0
              ? false
              : true,
        },
        prey: {
          ...state.prey,
          locked:
            state.upgrades.addHerbivore.upgradesTillUnlocked - 1 <= 0
              ? false
              : true,
        },
        carnivore: {
          ...state.carnivore,
          locked:
            state.upgrades.addCarnivore.upgradesTillUnlocked - 1 <= 0
              ? false
              : true,
        },
      };
    case "updateResource":
      return {
        ...state,
        [action.resourceType]: {
          ...state[action.resourceType],
          amount: state[action.resourceType].amount + 1,
        },
      };
    case "upgrade":
      return {
        ...state,
        water: {
          ...state.water,
          amount: state.water.amount - action.upgrade.cost.water,
          cooldown: action.upgrade.effect.water
            ? state.water.cooldown - action.upgrade.effect.water.amount
            : state.water.cooldown,
        },
        nutri: {
          ...state.nutri,
          amount: state.nutri.amount - action.upgrade.cost.nutri,
          cooldown: action.upgrade.effect.nutri
            ? state.nutri.cooldown - action.upgrade.effect.nutri.amount
            : state.nutri.cooldown,
        },
        plants: {
          ...state.plants,
          amount:
            state.plants.amount -
            action.upgrade.cost.plants +
            (action.upgrade.effect.plants
              ? action.upgrade.effect.plants.amount
              : 0),
        },
        prey: {
          ...state.prey,
          amount:
            state.prey.amount -
            action.upgrade.cost.prey +
            (action.upgrade.effect.prey
              ? action.upgrade.effect.prey.amount
              : 0),
        },
        carnivore: {
          ...state.carnivore,
          amount:
            state.carnivore.amount -
            action.upgrade.cost.carnivore +
            (action.upgrade.effect.carnivore
              ? action.upgrade.effect.carnivore.amount
              : 0),
        },
        rings:
          action.upgrade.id == "addRoots" ? [...state.rings, 1] : state.rings,
        upgrades: {
          ...state.upgrades,
          [action.upgrade.id]: {
            ...state.upgrades[action.upgrade.id],
            cost: {
              water:
                state.upgrades[action.upgrade.id].cost.water +
                state.upgrades[action.upgrade.id].cost.water * 0.15,
              nutri:
                state.upgrades[action.upgrade.id].cost.nutri +
                state.upgrades[action.upgrade.id].cost.nutri * 0.15,
              plants:
                state.upgrades[action.upgrade.id].cost.plants +
                state.upgrades[action.upgrade.id].cost.plants * 0.15,
              prey:
                state.upgrades[action.upgrade.id].cost.prey +
                state.upgrades[action.upgrade.id].cost.prey * 0.15,
              carnivore:
                state.upgrades[action.upgrade.id].cost.carnivore +
                state.upgrades[action.upgrade.id].cost.carnivore * 0.15,
            },
            isLocked: false,
          },
        },
      };

    default:
      throw new Error();
  }
}

export default function Start() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <div className="w-full flex h-[100vh]">
      <div className="m-12 w-full flex relative">
        <Recources state={state} dispatch={dispatch} />
        <TreeOfLife state={state} />
      </div>
      <UpgradeBox state={state} dispatch={dispatch} />
      <Overlay />
    </div>
  );
}
