export function naiveRound(num: number, decimalPlaces: number) {
  var p = Math.pow(10, decimalPlaces);
  return Math.round(num * p) / p;
}
